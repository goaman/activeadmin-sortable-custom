(function($) {
  $(document).ready(function() {
    $('.handle').closest('tbody').activeAdminSortable();
  });

  $.fn.activeAdminSortable = function() {
    this.sortable({
      update: function(event, ui) {
        var url = ui.item.find('[data-sort-url]').data('sort-url');
        var position = ui.item.index() + 1;
        var currentCategoryNumber = getCathegoryNumber(ui.item);
        var previousElement = ui.item;

        while (true) {
          previousElement = previousElement.prev();
          if (previousElement.length === 0) {
            break;
          }
          if (currentCategoryNumber != getCathegoryNumber(previousElement)) {
            console.log("not the same");
            position--;
          }
        }

        $.ajax({
          url: url,
          type: 'post',
          data: { position: position },
          success: function() { window.location.reload() }
        });
      }
    });

    this.disableSelection();
  }

  function getCathegoryNumber($el) {
		$a = $el.find(".col-parent_category a");
		if ($a.length > 0)
			return $a.attr("href").match(/([0-9]+)$/)[1];
  }
})(jQuery);
